﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Service.Contract;

namespace Backend.Node.Auth.Controllers
{
    [Route("api/[controller]")]
    public class AuthController : Controller, IAuthService
    {
        private readonly IAuthService _authService;

        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }
        public async Task<AuthUser> Authorize(AuthorizeModel model)
        {
            return await _authService.Authorize(model);
        }

        public async Task<AuthUser> Challenge(AuthenticateModel model)
        {
            return await _authService.Challenge(model);
        }

        public async Task CreateChallenge(AuthenticateModel model)
        {
            await _authService.CreateChallenge(model);
        }

        public async Task<bool> CreateLogin(CreateLoginModel model)
        {
            return await _authService.CreateLogin(model);
        }
    }
}
