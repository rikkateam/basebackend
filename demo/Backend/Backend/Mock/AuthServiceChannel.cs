﻿using Service.Contract;
using Service.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Mock
{
    public class AuthServiceProxy : IAuthService
    {
        private readonly IProxyChannelFactory _proxyChannelFactory;
        private readonly IProxyChannel _proxyChannel;

        public AuthServiceProxy(IProxyChannelFactory proxyChannelFactory)
        {
            _proxyChannelFactory = proxyChannelFactory;
            _proxyChannel = _proxyChannelFactory.CreateChannel(GetType());
        }

        public async Task<AuthUser> Authorize(AuthorizeModel model)
        {
            return await _proxyChannel.Request<AuthorizeModel, AuthUser>("Authorize", model);
        }

        public async Task<AuthUser> Challenge(AuthenticateModel model)
        {
            return await _proxyChannel.Request<AuthenticateModel, AuthUser>("Challenge", model);
        }

        public async Task CreateChallenge(AuthenticateModel model)
        {
            await _proxyChannel.Request<AuthenticateModel, object>("CreateChallenge", model);
        }

        public async Task<bool> CreateLogin(CreateLoginModel model)
        {
            return await _proxyChannel.Request<CreateLoginModel, bool>("CreateLogin", model);
        }
    }
}
