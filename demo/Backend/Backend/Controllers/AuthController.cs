﻿using Service.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Controllers
{
    public class AuthController
    {
        private readonly IAuthService _authService;

        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }

        public async Task<RegistrationDataModel> CreateChallenge(RegisterPhoneModel model)
        {
            var phone = model.CountryCode + model.Phone;
            var auth = new AuthenticateModel(0, phone);
            await _authService.CreateChallenge(auth);
            return null;
        }

        public async Task<RegistrationDataModel> Challenge(RegisterPhoneModel model)
        {
            var phone = model.CountryCode + model.Phone;
            var auth = new AuthenticateModel(0, phone, model.ConfirmCode);
            var result = await _authService.Challenge(auth);
            return new RegistrationDataModel(result.Token);
        }
    }

    public class RegisterPhoneModel
    {
        public string CountryCode { get; set; }
        public string Phone { get; set; }
        public string ConfirmCode { get; set; }
    }

    public class RegistrationDataModel
    {
        public RegistrationDataModel()
        {
        }
        public RegistrationDataModel(string token)
        {
            Token = token;
        }

        public string Token { get; set; }
    }
}
