﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Service.Core;

namespace BackendProducer.Controllers
{
    [Route("api/[controller]")]
    public class ProducerController : Controller, IProducerContract
    {
        public static Dictionary<Guid, ConfigurationNodeConsumer> Consumers = new Dictionary<Guid, ConfigurationNodeConsumer>();
        public static Dictionary<Guid, ConfigurationNodeService> Services = new Dictionary<Guid, ConfigurationNodeService>();

        [HttpPost]
        public async Task<IEnumerable<ConfigurationNodeService>> RegisterConsumer(ConfigurationNodeConsumer model)
        {
            if (!Consumers.ContainsKey(model.Id))
            {
                Consumers.Add(model.Id, model);
            }
            return Services.Values;
        }

        [HttpPost]
        public async Task<IEnumerable<ConfigurationNodeService>> RegisterService(ConfigurationNodeService model)
        {
            if (!Services.ContainsKey(model.Id))
            {
                Services.Add(model.Id, model);
            }
            return Services.Values;
        }
    }
}
