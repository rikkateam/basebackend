﻿using Dapper.Contrib.Extensions;
using System;

namespace ConsData.Core.Definition
{
    [Table("[User]")]
    public class UserModel
    {
        [ExplicitKey]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Image { get; set; }
        public bool Enabled { get; set; }
        public bool Lockedout { get; set; }
    }
}