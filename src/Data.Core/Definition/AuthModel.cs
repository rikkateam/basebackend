﻿using Dapper.Contrib.Extensions;
using System;

namespace ConsData.Core.Definition
{
    [Table("[Auth]")]
    public class AuthModel
    {
        [ExplicitKey]
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid LoginId { get; set; }
        public string Token { get; set; }
        public DateTime CreatedOn { get; set; }
        public string IpAddress { get; set; }
        public string Info { get; set; }
    }
}