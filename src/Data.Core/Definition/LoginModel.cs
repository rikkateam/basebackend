﻿using Dapper.Contrib.Extensions;
using System;

namespace ConsData.Core.Definition
{
    [Table("[Login]")]
    public class LoginModel
    {
        [ExplicitKey]
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public UserModel User { get; set; }
        public int Type { get; set; }
        public string Name { get; set; }
        public string Subject { get; set; }
        public string Challenge { get; set; }
        public string ChallengeParameter { get; set; }
        public int WrongCount { get; set; }
        public bool Enabled { get; set; }
        public bool Lockedout { get; set; }
        public bool NeedChange { get; set; }

    }
}
