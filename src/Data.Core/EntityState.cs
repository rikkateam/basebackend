﻿namespace Data.Core
{
    public enum EntityState
    {
        New, Loaded, Modified, Delete
    }
}
