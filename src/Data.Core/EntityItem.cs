﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data.Core
{
    public class EntityItem
    {
        private readonly IEntityRepository _repository;

        public EntityItem(IEntityRepository repository)
        {
            _repository = repository;
        }
        public EntityInfo EntityInfo { get; set; }
        public EntityState State { get; set; }
        public IDictionary<string, object> Values { get; set; }
        public object Model { get; set; }
        public T GetModel<T>()
        {
            return (T)Model;
        }

        public async Task Apply()
        {
            await _repository.Apply(this);
        }

        public async Task Apply<T>(T item)
        {
            await _repository.Apply(this);
        }

        internal int ValuesHashCode;
    }
}
