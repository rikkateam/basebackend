﻿using System;

namespace Data.Core
{
    public class ColumnInfo
    {
        public ColumnInfo(Type entityType, string name, Type type)
        {
            Name = name;
            Type = type;
            SetValue = entityType.GetProperty(name).SetValue;
            GetValue = entityType.GetProperty(name).GetValue; 
        }
        public string Name { get; set; }
        public Type Type { get; set; }

        internal Action<object, object> SetValue;
        internal Func<object, object> GetValue;
    }


}
