﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper.Contrib;
using Dapper;
using Dapper.Contrib.Extensions;
using System.Data;

namespace Data.Core
{
    public class Connect
    {
        private readonly string _connectionString;
        public Connect(IOptions<ConnectOptions> options)
        {
            _connectionString = options.Value.ConnectionString;
        }
        public SqlConnection GetConnection()
        {
            return new SqlConnection(_connectionString);
        }
    }

    public class ConnectOptions
    {
        public string ConnectionString { get; set; }
    }

    public class EntityRepository<T> : IEntityRepository, IEntityRepository<T> where T : class, new()
    {
        private readonly Connect _connect;
        protected readonly EntityInfo EntityInfo;
        public EntityRepository(Connect connect, EntityInfoStore entityInfoStore)
        {
            _connect = connect;
            EntityInfo = entityInfoStore.Get<T>();
        }
        public async Task<IEnumerable<EntityItem>> CallStoredProcedure(string name, object parameters = null)
        {
            using (var connection = _connect.GetConnection())
            {
                var items = await connection.QueryAsync<T>(name, parameters, commandType: CommandType.StoredProcedure);
                return items.Select(s => GetItem(s, EntityState.Loaded));
            }
        }

        public async Task<EntityItem> CallStoredProcedureFirst(string name, object parameters = null)
        {
            using (var connection = _connect.GetConnection())
            {
                var item = await connection.QueryFirstOrDefaultAsync<T>(name, parameters, commandType: CommandType.StoredProcedure);
                return GetItem(item, EntityState.Loaded);
            }
        }

        public async Task Insert(EntityItem item)
        {
            PrepareModel(item);
            using(var connection = _connect.GetConnection())
            {
                var model = item.GetModel<T>();
                await connection.InsertAsync(model);
            }
        }

        public async Task Update(EntityItem item)
        {
            PrepareModel(item);
            using (var connection = _connect.GetConnection())
            {
                var model = item.GetModel<T>();
                await connection.UpdateAsync(model);
            }
        }

        public async Task Delete(EntityItem item)
        {
            using (var connection = _connect.GetConnection())                
            {
                await connection.DeleteAsync(item);
            }
        }

        public async Task<EntityItem> Get(Guid id)
        {
            using (var connection = _connect.GetConnection())
            {
                var item = await connection.GetAsync<T>(id);
                if (item == null)
                    return null;
                return GetItem(item, EntityState.Loaded);
            }
        }

        public async Task<IEnumerable<EntityItem>> GetAll()
        {
            using (var connection = _connect.GetConnection())
            {
                var items = await connection.GetAllAsync<T>();
                return items.Select(s => GetItem(s, EntityState.Loaded));
            }
        }

        public EntityItem GetNew()
        {
            var item = new T();
            return GetItem(item, EntityState.New);
        }


        public async Task Apply(EntityItem item)
        {
            PrepareModel(item);
            switch (item.State)
            {
                case EntityState.New:
                    await Insert(item);
                    break;
                case EntityState.Loaded:
                case EntityState.Modified:
                    await Update(item);
                    break;
                case EntityState.Delete:
                    await Delete(item);
                    break;
            }
        }

        protected void PrepareModel(EntityItem item)
        {
            if(item.ValuesHashCode != item.Values.GetHashCode())
            {
                foreach(var field in item.Values.Where(f=> item.EntityInfo.Columns.ContainsKey(f.Key)))
                {
                    item.EntityInfo.Columns[field.Key].SetValue(item.Model, field.Value);
                }
            }
        }

        protected EntityItem GetItem(T model, EntityState state)
        {            
            var item = new EntityItem(this);
            item.EntityInfo = EntityInfo;
            item.State = state;
            item.Model = model;
            item.EntityInfo.SetValuesCollection(item);
            item.ValuesHashCode = item.Values.GetHashCode();
            return item;
        }

    }

    public interface IEntityRepository
    {
        Task<IEnumerable<EntityItem>> CallStoredProcedure(string name, object parameters = null);
        Task<EntityItem> CallStoredProcedureFirst(string name, object parameters = null);
        Task<IEnumerable<EntityItem>> GetAll();
        Task<EntityItem> Get(Guid id);
        Task Delete(EntityItem item);
        Task Update(EntityItem item);
        Task Insert(EntityItem item);

        Task Apply(EntityItem item);
        EntityItem GetNew();
    }

    public interface IEntityRepository<T> : IEntityRepository where T : class, new()
    {
    }

    public class Parameter
    {

        public Parameter()
        {
        }

        public Parameter(string name, object value)
        {
            Name = name;
            Value = value;
        }

        public string Name { get; set; }
        public object Value { get; set; }
    }

}
