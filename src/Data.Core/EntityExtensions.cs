﻿using System.Threading.Tasks;

namespace Data.Core
{
    public static class EntityExtensions
    {
        public static async Task Apply<T>(this T model, EntityItem item)
        {
            await item.Apply<T>(model);
        }
    }
}
