﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Data.Core
{

    public class EntityInfoStore
    {
        public static Dictionary<string, EntityInfo> Store = new Dictionary<string, EntityInfo>();

        public EntityInfo Get<T>()
        {
            var type = typeof(T);
            var name = type.Name;
            if (!Store.ContainsKey(name))
            {
                var info = new EntityInfo(name, type);

                var properties = type.GetProperties();
                foreach (var prop in properties.Where(w => w.CanRead && w.CanWrite))
                {
                    info.Columns.Add(prop.Name, new ColumnInfo(type, prop.Name, prop.PropertyType));
                }
                Store[name] = info;
            }
            return Store[name];

        }
    }
    public class EntityInfo
    {
        public EntityInfo(string name, Type type)
        {
            Name = name;
            Type = type;
            Columns = new Dictionary<string, ColumnInfo>();
        }

        public string Name { get; set; }
        public Type Type { get; set; }
        public IDictionary<string, ColumnInfo> Columns { get; set; }

        internal void SetValuesCollection(EntityItem item)
        {
            item.Values = new Dictionary<string, object>();
            foreach (var column in Columns)
            {
                try
                {
                    item.Values.Add(column.Key, column.Value.GetValue(item.Model));
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }
    }
}
