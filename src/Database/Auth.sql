﻿CREATE TABLE [dbo].[Auth]
(
	[Id] uniqueidentifier NOT NULL PRIMARY KEY,
	[UserId] uniqueidentifier not null, 
	[LoginId] uniqueidentifier not null, 
	[Token] varchar(250),
	[CreatedOn] datetime2 not null default(getdate()),
	[IpAddress] varchar(25),
	[Info] nvarchar(250),
	CONSTRAINT [FK_Auth_User] FOREIGN KEY ([UserId]) REFERENCES [User]([Id]),
	CONSTRAINT [FK_Auth_Login] FOREIGN KEY ([LoginId]) REFERENCES [Login]([Id])
)
