﻿CREATE TABLE [dbo].[User]
(
	[Id] uniqueidentifier NOT NULL PRIMARY KEY,
	[Name] varchar(250) null,
	[FirstName] varchar(250) null,
	[LastName] varchar(250) null,
	[MiddleName] varchar(250) null,
	[Phone] varchar(50) null,
	[Email] varchar(250) null,
	[Image] varchar(50) null,
	[Enabled] bit not null default(1),
	[Lockedout] bit not null default(0),
)
