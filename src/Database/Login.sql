﻿CREATE TABLE [dbo].[Login]
(
	[Id] uniqueidentifier NOT NULL PRIMARY KEY,
	[UserId] uniqueidentifier not null, 
	[Type] int not null,
	[Name] nvarchar(25),
	[Subject] varchar(250),
	[Challenge] varchar(500),
	[ChallengeParameter] varchar(500),
	[WrongCount] int not null default(0),
	[Enabled] bit not null default(1),
	[Lockedout] bit not null default(0),
	[NeedChange] int not null default(0),
	CONSTRAINT [FK_Login_User] FOREIGN KEY ([UserId]) REFERENCES [User]([Id])
)
