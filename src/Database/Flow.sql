﻿CREATE TABLE [dbo].[Flow]
(
	[Id] uniqueidentifier NOT NULL PRIMARY KEY,

	[CreatedOn] datetime2 not null default(getdate()),
	[ModifiedOn] datetime2 not null default(getdate()),
	[CreatedBy] varchar(500) NULL default('{"id": "{00000000-0000-0000-0000-000000000000}", "name": "System"}'),
	[ModifiedBy] varchar(500) NULL default('{"id": "{00000000-0000-0000-0000-000000000000}", "name": "System"}'),

	[Name] varchar(250) not null,
	[Point] uniqueidentifier not null,
	[PointName] varchar(250) not null,	
	[FlowSchema] varchar(max) not null,
	[Data] varchar(max) not null,

	[Error] varchar(max) null

)
