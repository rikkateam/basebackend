﻿CREATE PROCEDURE [dbo].[sp_checkPasswordChallenge]
	@type int,
	@subject nvarchar(250),
	@challenge varchar(500)
AS
	SELECT TOP(1) * FROM [Login] WHERE [Type] = @type AND [Subject] = @subject AND [Challenge] = @challenge;
RETURN 0
