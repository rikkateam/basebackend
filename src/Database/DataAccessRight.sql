﻿CREATE TABLE [dbo].[DataAccessRight]
(
	[Id] uniqueidentifier NOT NULL PRIMARY KEY,

	[CreatedOn] datetime2 not null default(getdate()),
	[ModifiedOn] datetime2 not null default(getdate()),
	[CreatedBy] varchar(500) NULL default('{"id": "{00000000-0000-0000-0000-000000000000}", "name": "System"}'),
	[ModifiedBy] varchar(500) NULL default('{"id": "{00000000-0000-0000-0000-000000000000}", "name": "System"}'),

	[RightsObject] varchar(50) NOT NULL,
	[RightsSubject] uniqueidentifier NULL,
	[Target] uniqueidentifier NULL,
	[Acl] int NOT NULL
)
