﻿using System;
using System.Threading.Tasks;

namespace Service.Contract
{
    public interface IAuthService
    {
        Task CreateChallenge(AuthenticateModel model);
        Task<AuthUser> Challenge(AuthenticateModel model);
        Task<AuthUser> Authorize(AuthorizeModel model);
        Task<bool> CreateLogin(CreateLoginModel model);
    }

    public class CreateLoginModel
    {
        public CreateLoginModel()
        {

        }
        public CreateLoginModel(Guid userId, int type, string subject, string challenge)
        {
            UserId = userId;
            Type = type;
            Subject = subject;
            Challenge = challenge;
        }

        public Guid UserId { get; set; }
        public int Type { get; set; }
        public string Subject { get; set; }
        public string Challenge { get; set; }
    }

    public class AuthenticateModel
    {
        public AuthenticateModel()
        {

        }
        public AuthenticateModel(int type, string subject, string challenge = null)
        {
            Type = type;
            Subject = subject;
            Challenge = challenge;
        }

        public  int Type { get; set; }
        public string Subject { get; set; }
        public string Challenge { get; set; }
    }
    public class AuthorizeModel
    {
        public string Token { get; set; }
    }
    public class AuthUser
    {
        public AuthUser()
        {

        }
        public AuthUser(Guid id, Guid userId, string token, string name)
        {
            Id = id;
            UserId = userId;
            Token = token;
            Name = name;
        }

        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string Token { get; set; }
        public string Name { get; set; }
    }
}
