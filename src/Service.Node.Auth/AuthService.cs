﻿using ConsData.Core.Definition;
using Data.Core;
using Microsoft.Extensions.Options;
using Service.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Service.Node.Auth
{
    public class AuthService : IAuthService
    {
        private readonly Dictionary<int, IChallengeProvider> _challengeCollection;
        private readonly IEntityRepository<LoginModel> _loginRepository;
        private readonly IEntityRepository<AuthModel> _authRepository;

        public AuthService(ICollection<IChallengeProvider> challengeCollection,
            IEntityRepository<LoginModel> loginRepository,
            IEntityRepository<AuthModel> authRepository)
        {
            _challengeCollection = challengeCollection.ToDictionary(k=>k.Type, v=> v);
            _loginRepository = loginRepository;
            _authRepository = authRepository;
        }

        public Task CreateChallenge(AuthenticateModel model)
        {
            throw new NotImplementedException();
        }

        public async Task<AuthUser> Challenge(AuthenticateModel model)
        {
            var provider = _challengeCollection[model.Type];
            var result = await provider.Check(model.Subject, model.Challenge);
            if (result == null)
                return null;
            var login = await _loginRepository.Get(result.Value);
            var loginModel = login.GetModel<LoginModel>();
            var auth = _authRepository.GetNew();
            var authModel = auth.GetModel<AuthModel>();
            authModel.UserId = loginModel.UserId;
            authModel.LoginId = loginModel.Id;
            authModel.Token = GenerateToken();
            await authModel.Apply(auth);
            return new AuthUser(loginModel.Id, loginModel.UserId, authModel.Token, loginModel.Name);
        }

        public async Task<AuthUser> Authorize(AuthorizeModel model)
        {
            var auth = await _authRepository.CallStoredProcedureFirst("sp_findAuthToken", new { token = model.Token });
            var authModel = auth.GetModel<AuthModel>();
            var login = await _loginRepository.Get(authModel.LoginId);
            var loginModel = login.GetModel<LoginModel>();
            return new AuthUser(authModel.LoginId, authModel.UserId, authModel.Token, loginModel.Name);
        }

        public async Task<bool> CreateLogin(CreateLoginModel model)
        {
            var login = _loginRepository.GetNew();
            var loginModel = login.GetModel<LoginModel>();

            loginModel.UserId = model.UserId;
            loginModel.Subject = model.Subject;
            await loginModel.Apply(login);

            var provider = _challengeCollection[model.Type];
            await provider.ApplyToLogin(loginModel.Id, model.Challenge);

            return true;
        }

        private string GenerateToken()
        {
            var random = new Random(Guid.NewGuid().GetHashCode());
            const int size = 64;
            const string alphabet = "abcdefghijklmnopqrstuvwxyz1234567890-_";
            return string.Join("", Enumerable.Range(0, size).Select(s => alphabet[random.Next(alphabet.Length)]));
        }

    }

    public interface IChallengeProvider
    {
        int Type { get; set; }
        string Name { get; set; }
        Task<Guid?> Check(string subject, string challenge);
        Task ApplyToLogin(Guid id, string challenge);
    }

    public class PasswordChallengeProvider : IChallengeProvider
    {
        private readonly IEntityRepository<UserModel> _userRepository;
        private readonly IEntityRepository<LoginModel> _loginRepository;
        private readonly PasswordChallengeProviderOptions _options;
        public PasswordChallengeProvider(IEntityRepository<UserModel> userRepository, 
            IEntityRepository<LoginModel> loginRepository,            
            IOptions<PasswordChallengeProviderOptions> options)
        {
            _userRepository = userRepository;
            _loginRepository = loginRepository;
            _options = options.Value;
        }

        public int Type { get; set; }
        public string Name { get; set; }

        public async  Task<Guid?> Check(string subject, string challenge)
        {
            var login = await _loginRepository.CallStoredProcedureFirst("[sp_findLogin]", new{ subject = subject });
            if(login == null)
                return null;
            var loginModel = login.GetModel<LoginModel>();
            var hash = ComputeHash(challenge, loginModel.ChallengeParameter);
          
            var result = await  _loginRepository.CallStoredProcedureFirst("[sp_checkPasswordChallenge]", new
            {
                type = Type,
                subject,
                challenge = hash
            });
            if (result == null)
                return null;
            var resultModel = result.GetModel<LoginModel>();
            return resultModel.Id;

        }                

        protected HashAlgorithm GetChiper()
        {
            var chiper = SHA384.Create();
            return chiper;
        }

        protected string ComputeHash(string password, string salt)
        {
            var chiper = GetChiper();
            var subject = password + salt + _options.ChiperGeneralSalt;
            var target = chiper.ComputeHash(Encoding.UTF8.GetBytes(subject));
            return Encoding.UTF8.GetString(target);

        }

        public async Task ApplyToLogin(Guid id, string challenge)
        {
            var login = await _loginRepository.Get(id);
            var loginModel = login.GetModel<LoginModel>();
            loginModel.ChallengeParameter = GenerateSalt();
            loginModel.Challenge = ComputeHash(challenge, loginModel.ChallengeParameter);
            await loginModel.Apply(login);

        }

        private string GenerateSalt()
        {
            var random = new Random(Guid.NewGuid().GetHashCode());
            const int size = 64;
            const string alphabet = "abcdefghijklmnopqrstuvwxyz1234567890-_";
            return string.Join("", Enumerable.Range(0, size).Select(s => alphabet[random.Next(alphabet.Length)]));
        }
    }

    public class PasswordChallengeProviderOptions
    {
        public string ChiperGeneralSalt { get; set; }
    }
}

