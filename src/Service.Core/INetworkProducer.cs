﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Service.Core
{
    public interface INetworkProducer
    {
        IDictionary<string, List<ConfigurationNodeService>> Services { get; set; }
        ConfigurationNodeService GetRandomService(string name);
        Task RegisterService(string service, string machineName = null, string address = null);
        Task RegisterConsumer(IEnumerable<string> dependencies = null);
    }
}