﻿using System;

namespace Service.Core
{
    public class ConfigurationNodeService : ConfigurationNode
    {
        public ConfigurationNodeService()
        {

        }
        public ConfigurationNodeService(Guid id, string service, string machineName, string address) : base(id)
        {
            Service = service;
            MachineName = machineName;
            Address = address;
        }

        public string Service { get; set; }
        public string MachineName { get; set; }
        public string Address { get; set; }
    }

}

