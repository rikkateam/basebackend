﻿using System.Threading.Tasks;

namespace Service.Core
{
    public interface IProxyChannel
    {
        Task<TResponse> Request<TSend, TResponse>(string name, TSend payload);
    }

}

