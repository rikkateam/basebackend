﻿using System;

namespace Service.Core
{
    public class ConfigurationNode
    {
        public ConfigurationNode()
        {

        }

        public ConfigurationNode(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; set; }
    }

}

