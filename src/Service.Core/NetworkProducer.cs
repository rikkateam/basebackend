﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;

namespace Service.Core
{
    public class NetworkProducer : INetworkProducer
    {
        private NetworkProducerOptions _options;

        private static Guid Id = Guid.NewGuid();
        public NetworkProducer(IOptions<NetworkProducerOptions> options)
        {
            _options = options.Value;
            Services = new Dictionary<string, List<ConfigurationNodeService>>();
        }
        public IDictionary<string, List<ConfigurationNodeService>> Services { get; set; }

        public ConfigurationNodeService GetRandomService(string name)
        {
            var random = new Random(Guid.NewGuid().GetHashCode());
            if (!Services.ContainsKey(name))
            {
                throw new Exception("no service");
            }
            var pool = Services[name];
            var index = random.Next(pool.Count);
            return pool[index];
        }

        public async Task RegisterService(string service, string machineName = null, string address = null)
        {
            var client = new HttpClient();
            var request = new ConfigurationNodeService(Id, service, machineName ?? Environment.MachineName, address);
            var response = await client.PostAsync(_options.ProducerHost, new StringContent(request.Serialize(), Encoding.UTF8, "application/json"));
        }

        public async Task RegisterConsumer(IEnumerable<string> dependencies = null)
        {
            var client = new HttpClient();
            var request = new ConfigurationNodeConsumer(Id, dependencies);
            var response = await client.PostAsync(_options.ProducerHost, new StringContent(request.Serialize(), Encoding.UTF8, "application/json"));
            var json = await response.Content.ReadAsStringAsync();
            var nodes = json.Deserialize<IEnumerable<ConfigurationNodeService>>();
            Services = nodes.ToDictionary(k => k.Service, v => nodes.Where(w => w.Service == v.Service).ToList());
        }

    }

}

