﻿using System;

namespace Service.Core
{
    public interface IProxyChannelFactory
    {
        IProxyChannel CreateChannel(Type type);
    }

}

