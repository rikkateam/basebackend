﻿namespace Service.Core
{
    public class ChannelFault
    {
        public ChannelFault()
        {

        }
        public ChannelFault(string fault, string typeCode, string code)
        {
            Fault = fault;
            TypeCode = typeCode;
            Code = code;
        }

        public string Fault { get; set; }
        public string TypeCode { get; set; }
        public string Code { get; set; }
    }

}

