﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Service.Core
{
    public class HttpProxyChannel : IProxyChannel
    {
        private readonly string _address;

        public HttpProxyChannel(string address)
        {
            this._address = address;
        }

        protected virtual HttpClient GetClient()
        {
            return new HttpClient();
        }

        public async Task<TResponse> Request<TSend, TResponse>(string name, TSend payload)
        {
            var client = GetClient();
            var json = payload.Serialize();
            var url = $"{_address}/{name}";
            var response = await client.PostAsync(url, new StringContent(json, Encoding.UTF8, "application/json"));
            if ((int)response.StatusCode >= 500)
            {
                var faultJson = await response.Content.ReadAsStringAsync();
                var fault = faultJson.Deserialize<ChannelFault>();
                throw new NotImplementedException("pass fault exception");
            }
            var responseJson = await response.Content.ReadAsStringAsync();
            var result = responseJson.Deserialize<TResponse>();
            return result;
        }
    }

}

