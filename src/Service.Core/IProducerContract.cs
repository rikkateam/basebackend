﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Service.Core
{
    public interface IProducerContract
    {
        Task<IEnumerable<ConfigurationNodeService>> RegisterConsumer(ConfigurationNodeConsumer model);
        Task<IEnumerable<ConfigurationNodeService>> RegisterService(ConfigurationNodeService model);
    }

}

