﻿using System;
using System.Collections.Generic;

namespace Service.Core
{
    public class ConfigurationNodeConsumer : ConfigurationNode
    {
        public ConfigurationNodeConsumer()
        {

        }
        public ConfigurationNodeConsumer(Guid id, IEnumerable<string> dependencies) : base(id)
        {
            Dependencies = dependencies;
        }

        IEnumerable<string> Dependencies { get; set; }
    }

}

