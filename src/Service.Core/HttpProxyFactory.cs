﻿using System;

namespace Service.Core
{
    public class HttpProxyFactory : IProxyChannelFactory
    {
        private readonly INetworkProducer _networkProducer;

        public HttpProxyFactory(INetworkProducer networkProducer)
        {
            this._networkProducer = networkProducer;
        }
        public IProxyChannel CreateChannel(Type type)
        {
            var service = _networkProducer.GetRandomService(type.Name);
            var channel = new HttpProxyChannel(service.Address);
            return channel;
        }
    }

}

