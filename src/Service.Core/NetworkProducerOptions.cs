﻿namespace Service.Core
{
    public class NetworkProducerOptions
    {
        public string Key { get; set; }
        public string ProducerHost { get; set; }
    }

}

