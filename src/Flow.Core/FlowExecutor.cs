﻿//using ConsoleApp1.Data.Definition;
//using Microsoft.AspNetCore.NodeServices;
//using Newtonsoft.Json;
//using Newtonsoft.Json.Serialization;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace ConsoleApp1.Flow
//{
//    public class FlowExecutor
//    {
//        private readonly INodeServices _nodeServices;
//        protected readonly IEntityRepository<FlowModel> _flowRepository;
//        public delegate Task<Exception> FlowPointExecutor(MetaFlowSchema flowSchema, MetaPoint point, EntityItem flow, object model);
//        private readonly IDictionary<string, FlowPointExecutor> _pointExecutors;
//        public const string FlowExecutionSystemModule = "FlowExecutionSystemModule";
//        public FlowExecutor(INodeServices nodeServices, IEntityRepository<FlowModel> flowRepository)
//        {
//            _nodeServices = nodeServices;
//            _flowRepository = flowRepository;
//            _pointExecutors = new Dictionary<string, FlowPointExecutor>
//            {
//                {"MetaProgrammabilityPoint",ExecJsPoint }
//            };
//        }

//        public async Task Start(MetaFlowSchema flowSchema, IDictionary<string, object> parameters)
//        {
//            var item = _flowRepository.GetNew();
//            var model = item.GetModel<FlowModel>();
//            model.Name = flowSchema.Name;
//            model.Point = flowSchema.Start;
//            var startPoint = flowSchema.GetPoint(flowSchema.Start);
//            model.PointName = startPoint.Name;
//            model.FlowSchema = flowSchema.Serialize();
//            var data = new Dictionary<string, object>(flowSchema.Parameters.ToDictionary(k => k.Name, v => v.Value));
//            foreach (var parameter in parameters)
//            {
//                data[parameter.Key] = parameter.Value;
//            }
//            model.Data = data.Serialize();
//            await model.Apply(item);
//        }


//        public async Task Continue(Guid id, MetaFlowSchema flowSchema)
//        {
//            var item = await _flowRepository.Get(id);
//            var model = item.GetModel<FlowModel>();
//            var point = flowSchema.GetPoint(model.Point);
//            var function = _pointExecutors[point.Type];
//            var error = await function.Invoke(flowSchema, point, item, model);
//            if (error != null)
//            {
//                model.Error = error.Message;
//                await model.Apply(item);
//                return;
//            }
//            else
//            {
//                await model.Apply(item);
//            }

//            var next = await GetNext(flowSchema, point, item);
//            model.PointName = next.Name;
//            model.Point = next.Id;
//            await model.Apply(item);
//        }

//        protected async Task<MetaPoint> GetNext(MetaFlowSchema flowSchema, MetaPoint point, EntityItem flow)
//        {
//            var links = point.GetLinks(flowSchema);
//            var conditions = links.Where(w => w.Type == MetaLinkType.Condition);
//            var basic = links.FirstOrDefault(f => f.Type == MetaLinkType.Basic);
//            if (conditions.Any())
//            {
//                foreach (var condition in conditions)
//                {
//                    var result = await _nodeServices.InvokeExportAsync<bool>(FlowExecutionSystemModule, "CheckConditions", flow.Model);
//                    if (result)
//                    {
//                        return flowSchema.GetPoint(condition.To);
//                    }
//                }
//            }
//            return flowSchema.GetPoint(basic.To);
//        }

//        protected async Task<Exception> ExecJsPoint(MetaFlowSchema flowSchema, MetaPoint point, EntityItem flow, object model)
//        {
//            var progPoint = (MetaProgrammabilityPoint)point;
//            try
//            {
//                var result = await _nodeServices.InvokeExportAsync<bool>(flowSchema.ModuleName, progPoint.ExportedFunctionName, model);
//            }
//            catch (Exception exception)
//            {
//                return exception;
//            }
//            return null;
//        }
//    }



//    public static class Json
//    {
//        public static string Serialize<T>(this T obj)
//        {
//            var json = JsonConvert.SerializeObject(obj, new JsonSerializerSettings
//            {
//                ContractResolver = new CamelCasePropertyNamesContractResolver()
//            });
//            return json;
//        }

//        public static T Deserialize<T>(this string json)
//        {
//            var obj = JsonConvert.DeserializeObject<T>(json, new JsonSerializerSettings
//            {
//                ContractResolver = new CamelCasePropertyNamesContractResolver()
//            });
//            return obj;
//        }
//    }
//}
