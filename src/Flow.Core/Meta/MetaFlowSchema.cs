﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace ConsoleApp1.Flow
//{
//    public class MetaFlowSchema
//    {
//        public Guid Id { get; set; }
//        public string Name { get; set; }
//        public int Version { get; set; }
//        public IList<MetaPoint> Points { get; set; }
//        public IList<MetaLink> Links { get; set; }
//        public IList<MetaParameter> Parameters { get; set; }
//        public Guid Start { get; set; }
//        public string ModuleName { get; set; }
//    }
//    public class MetaLink
//    {
//        public Guid From { get; set; }
//        public Guid To { get; set; }
//        public string Name { get; set; }
//        public MetaLinkType Type { get; set; }
//        public string Condition { get; set; }
//    }
//    public enum MetaLinkType
//    {
//        Basic,
//        Condition
//    }
//    public abstract class MetaPoint
//    {
//        public Guid Id { get; set; }
//        public string Name { get; set; }
//        public string Type { get; set; }
//    }

//    public class MetaProgrammabilityPoint: MetaPoint
//    {        
//        public string ExportedFunctionName { get; set; }
//    }
//    public class MetaDataReadPoint : MetaPoint
//    {
//        public string Entity { get; set; }
//    }

//    public class MetaParameter
//    {
//        public Guid Id { get; set; }
//        public string Name { get; set; }
//        public string Type { get; set; }
//        public object Value { get; set; }
//    }

//    public static class MetaExtensions
//    {
//        public static MetaParameter GetParameter(this MetaFlowSchema flow, Guid id)
//        {
//            return flow.Parameters.FirstOrDefault(f => f.Id == id);
//        }
//        public static MetaPoint GetPoint(this MetaFlowSchema flow, Guid id)
//        {
//            return flow.Points.FirstOrDefault(f => f.Id == id);
//        }
//        public static IEnumerable<MetaLink> GetLinks(this MetaPoint point, MetaFlowSchema flow)
//        {
//            return flow.Links.Where(w => w.From == point.Id);
//        }
//    }
//}
